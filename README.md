# Dulbert : the tales of a hero or an idiot?

[Untitled Game Jam #16](https://itch.io/jam/untitled-game-jam-16) entry for survival theme it is a visual novel which can the user help the mc dulbert become a hero or die like an idiot in the war
Game Demo : https://rossvincentdiaz12.gitlab.io/dulbert-the-tales-of-a-hero-or-an-idiot/

## Built With

* [Ren`py](https://www.renpy.org/) 



## Contributing

Please read [CONTRIBUTING.md](https://gist.github.com/PurpleBooth/b24679402957c63ec426) for details on our code of conduct, and the process for submitting pull requests to us.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details