﻿# The script of the game goes in this file.

# Declare characters used by this game. The color argument colorizes the
# name of the character.

define a = Character("Story Teller")
define d = Character("Dulbert")
define s = Character("Soldier")
define p = Character("Paul")

image house = "house.png"
image town = "town.png"
image forest = "forest.png"

image dulbert ="dulbert.png"
image paul ="paul.png"
image soldier ="soldier.png"

# The game starts here.

label start:

    # Show a background. This uses a placeholder by default, but you can
    # add a file (named either "bg room.png" or "bg room.jpg") to the
    # images directory to show it.



    "Long ago in the kingdom of Maia there was a boy named Dulbert. "

    "Dulbert has a lot of potential talents but...."

    "he was full of himself he was narcissistic, lazy and selfish person he has short temper"

    "he looks like brave and strong person but he was a softy and coward guy."

    "will Dulbert change and be the greatest hero or stay being a loser"

    "One day……"

    "Afternoon Dulbert was wake up by a noise outside of house"

    scene house
    show dulbert

    d "ughh.. What is that noise outside?”"

    scene town
    show soldier

    "It is a sound of a horse running and it is announcing important news to the citizen of the kingdom."

    s "Call the attention of the citizen of Maia."

    s "Our king Bogart the 1st calling the aid of the young men of this kingdom."

    s "The evil wizard king in the northern kingdom has assembled his army "

    s "army to invade the southern kingdoms including our beloved kingdom Maia"

    scene house
    show dulbert

    "Dulbert tremble of what he heard he don’t want to risk his life he thinking it’s not a big problem."

    "So he ignore it and go back to sleep"

    "but.........."

    "there was a people knocking on his door."

    d "uuuuuuuuuuuughhhhhh……. Who is it!?"

    p "It’s me Dulbert"

    "It was Paul Dulbert’s neighbour he was always positive and try to encourage Dulbert to participate on social event or work."

    d "Ohh Paul come in"

    show dulbert at left
    show paul at right

    p "Did you hear about the drafting in the army their preparing for a war!"

    d "No……. I was on the backyard……"

    p "Don’t worry you can pick any position you want to be assign in the army come on let’s go check it out."

    scene town

    "Dulbert  didn’t want to go but Paul insist him to come so he does not have a choice."

    "Paul and Dulbert when to the town square."

    show dulbert at left
    show paul at right

    p "There Dulbert you can choose which job you want to be assign in the army"

    "Dulbert look at the list "

    menu:
        "Infantry men":
            jump im
        "Scout":
            jump bs


    # This ends the game.

    return

label im:

    d "Maybe infantry men"

    p "Really..theres a lot of walking there and you have to help setting up the camp"

    d "……………………….."

    p "I will go for the scout I will infiltrate the enemies bases and deliver secret assets to the kingdom "

    p "that’s a fun and easy job"

    menu:
        "Ending 1":
            jump imone
        "Ending 2":
            jump imtwo
        "Ending 2":
            jump imthree

    return

label bs:

    d "Maybe a Scout"

    p "Really..it s dangerous you will have to go to the enemys camp and gather information"

    p "you know what I will go for the infrantry"

    p "I will be in front line on the battle,"

    p "Where there’s a higher chance to be a war hero"

    menu:
        "Ending 1":
            jump bsone
        "Ending 2":
            jump bstwo
        "Ending 2":
            jump bsthree

    return

label imone:

    scene forest

    "Dulbert cant no longer live the harsh environment in the front line"

    "so he decided to desert his post but the other soldier caught him and he was executed by deserting his post."

    return

label imtwo:

    scene forest

    "Dulbert fishiest the training and set upping the camp but he did not take the training seriously"

    "he slacks of and try to skips training"

    "So in the battle time Dulbert has no skill developed during the training and immediately killed after the battle started."

    return

label imthree:

    scene town
    show dulbert

    "Dulbert work hard on his training he assembles the front line sturdy."

    "Dulbert became strong and brave he became one of person leading the army"

    "When the battle start Dulbert was one who charge 1st into the battle field."

    "No one can’t stop him and the enemy was scared and defeated by the will power of Dulbert."

    "He was declared the war hero of Maia the bravest man in the kingdom."

    return

label bsone:

    scene forest

    "Dulbert gets afraid and he decided to desert his post "

    "but the other soldier caught him and he was executed by deserting his post."

    return

label bstwo:

    scene forest

    "Dulbert did not focus of the scoting job he usually dooze off because of that he did not deliver the message that the enemy will be attack so the kingdom of Maia was defeated"

    return

label bsthree:

    scene forest

    "Dulbert give his all in the scouting job he provided important intel about the enemy and its strategies."

    "leading the kingdom of Maia victory. He was declared the war hero of Maia the bravest man in the kingdom."


    return
